package main

import (
	"fmt"
	"strings"
)

func findFirstStringInBracket(str string) string {
	i := strings.IndexByte(str, '(')
	if i < 0 {
		return ""
	}
	i++
	j := strings.IndexByte(str[i:], ')')
	if j < 0 {
		return ""
	}
	finalString := str[i : i+j]
	fmt.Println(finalString)

	return finalString
}

func main() {
	findFirstStringInBracket("Un(conscious)")
}
